﻿import React, { Component } from 'react'
import './MessageComponent.scss'
import axios from 'axios';

class MessageComponent extends Component
{

    constructor(props)
    {
        super(props)
        this.state = {
            title: '',
            body: ''
        }
    }

    onChange = (e) => {
        // update state
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit = (e) =>
    {
        console.log(e)
        e.preventDefault()
        // get form data out of state
        const { title, body } = this.state;

        axios.post('/api/Message', { title, body }).then((result) => { })
        window.location.reload(false)
    }

    render()
    {
        const { title, body } = this.state;
        return (
            <div className="message">
                <h3> Message App </h3>
                <form onSubmit={this.onSubmit} method="GET">
                    <label>
                        Title:<br />
                        <input className="message__title" type="text" name="title" onChange={this.onChange}/>
                    </label>
                    <label>
                        body: <br />
                        <textarea className="message__body" name="body" rows="4" cols="50" onChange={this.onChange}>

                        </textarea>
                    </label>
                    <input className="message__button" type="submit" value="Send" />
                </form>
            </div>
        )
    }
}

export default MessageComponent