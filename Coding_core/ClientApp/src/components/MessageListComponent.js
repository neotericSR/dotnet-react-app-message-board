﻿import React from 'react'
import { Link } from "react-router-dom"
import './MessageListComponent.scss'


const MessageListComponent = (props) =>
    (
        <React.Fragment>
            <div className="messageList">
                <h3 className="messageList__title">Message List</h3>
                <table className="messageList__body">
                    <thead>
                    <tr><th>ID</th> 
                    <th>Title</th> 
                     <th>Body </th>
                     <th>Date</th></tr>
                    </thead>
                    <tbody>{props.messages && props.messages.map(m =>
                            <tr key={m.id}><td> {m.id}</td>
                                <td>{m.title}</td>
                                <td>{m.body}</td>
                                <td>{m.date}</td></tr>
                        )}</tbody>
                </table>
             </div>
        </React.Fragment>
    );

export default MessageListComponent