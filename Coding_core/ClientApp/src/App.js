import React, { Component } from 'react'
import MessageComponent from './components/MessageComponent'
import MessageListComponent from './components/MessageListComponent'
import axios from 'axios'

import './custom.css'

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {data: []}
    }
    componentDidMount()
    {
        axios.get('/api/Messages').then(response => { this.setState({ data: response.data }) }).catch(error => { console.error(error)})
    }
    render() {
      return (
          <div className="container">
              <MessageComponent />
              <MessageListComponent messages={this.state.data}/>
        </div>
    );
  }
}
