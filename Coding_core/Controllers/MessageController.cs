﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Coding_core.Controllers
{
    [EnableCors]
    [Route("/api/Message")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        readonly string filePath = Path.GetFullPath(".\\ClientApp\\src\\data\\data.json");
        [Route("/api/Messages")]
        public List<Message> GetAll() 
        {
            List<Message> items;
            using (StreamReader r = new StreamReader(filePath))
            {
                string json = r.ReadToEnd();
                items = JsonConvert.DeserializeObject<List<Message>>(json);
            }

            return items;
        }
        [Route("/api/Message")]
        public bool Post(Message message) 
        {
            try
            {
                // Read existing json data
                var jsonData = System.IO.File.ReadAllText(filePath);
                // De-serialize to object or create new list
                var messageList = JsonConvert.DeserializeObject<List<Message>>(jsonData)
                                      ?? new List<Message>();
                
                int maxId = messageList.Count > 0 ? messageList.Max(i => i.ID) : 0;
                message.ID = ++maxId;
                message.Date = DateTime.Now.ToString(); 
                // Add message
                messageList.Add(message);

                // Update json data string
                jsonData = JsonConvert.SerializeObject(messageList);
                System.IO.File.WriteAllText(filePath, jsonData);
                return true;
            }
            catch (Exception e) {
                throw e;
            }
          
        }
    }
}
